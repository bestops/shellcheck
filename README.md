# Docker shellcheck

[![pipeline status](https://gitlab.com/bestops/shellcheck/badges/master/pipeline.svg)](https://gitlab.com/bestops/shellcheck/commits/master)
[![coverage report](https://gitlab.com/bestops/shellcheck/badges/master/coverage.svg)](https://gitlab.com/bestops/shellcheck/commits/master)

[![image](https://dockeri.co/image/bestops/shellcheck?&kill_cache=1)](https://hub.docker.com/u/bestops/shellcheck)

## A docker **shellcheck** built for [amd64, arm32v6, arm32v7, arm64v8, i386](https://hub.docker.com/u/bestops/shellcheck/tags?page=1)

- [Alpine Linux](https://hub.docker.com/_/alpine/)
  - [SSL Package installs](https://justi.cz/security/2018/09/13/alpine-apk-rce.html)
  - [Verify packages](a)
- [ShellCheck](https://github.com/koalaman/shellcheck) - A static analysis tool for shell scripts
  - Built from source, [apk packages](https://pkgs.alpinelinux.org/packages?name=shellcheck&branch=v3.11) only exist for x86_64
- [tini](https://github.com/krallin/tini) - A tiny but valid init for containers
- [best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices) and methods for building efficient images.

## A Secure gitlab CI / CD process for reliable releases

### Contributing clean code

- precommit rules
- gpg signed commits

### Build and review process

- [Docker Buildx](https://docs.docker.com/buildx/working-with-buildx/) for cross platform support
- Image security review with [clair](https://github.com/arminc/clair-scanner)
- Static code analysis from [gitlabs tools](https://docs.gitlab.com/ee/user/application_security/sast/)

### Signed releases

- [Docker Content Trust (DCT)](https://docs.docker.com/engine/security/trust/content_trust/) for image signing and verification

## Usage

Full shellcheck guide can be found in [koalaman/shellcheck](https://github.com/koalaman/shellcheck)

### Tests

```bash
docker run -it --rm -v "$(PWD):/data:ro" bestops/shellcheck:latest ./tests/*.sh
```

### Ignoring rules

```bash
docker run -it --rm -v "$(PWD):/data:ro" -e SHELLCHECK_OPTS="-e SC2230 -e SC2034" bestops/shellcheck:latest ./tests/*.sh
```

### Gitlab CI

```bash
test:
  image: koalaman/shellcheck:latest
  stage: test
  script:
  - testScript.sh
```

## Supported architectures

[amd64, arm32v6, arm32v7, arm64v8, i386, ppc64le](https://hub.docker.com/u/bestops/shellcheck/tags?page=1)

```bash
docker inspect bestops/shellcheck:latest | jq
docker manifest inspect bestops/shellcheck:latest | jq
```

## License

The package is distributed under the [MIT](LICENSE) license.
