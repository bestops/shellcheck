# Contributing

Interested in contributing? Great! Here are some suggestions to make it a good experience:

- Start by opening an issue, whether to identify a problem or outline a change.
  - That issue should be used to discuss the situation and agree on a plan of action before writing code or sending a pull request.
  - Maybe the problem isn't really a problem, or maybe there are more things to consider. If so, it's best to realize that before spending time and effort writing code that may not get used.
- Match the coding style of the files you edit. Although everyone has their own preferences and opinions, a pull request is not the right forum to debate them.
- Do not add new dependencies to the Dockerfile. The excellent shellcheck and tini are this project's only dependencies.
- Add tests for all new/changed functionality.
- Test positive and negative scenarios.
- Try to break the new code now, or else it will get broken later.
- Run tests before sending a pull request via `make test`.
- Lint before sending a pull request by running `make lint`. There should be no issues.
