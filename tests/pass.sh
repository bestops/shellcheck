#!/bin/bash
cd generated_files || exit

# For functions, you may want to use return:
func(){
  cd foo || return
  do_something
}
