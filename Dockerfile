FROM --platform=$BUILDPLATFORM debian:stable AS build
WORKDIR /opt

# Install OS deps
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
      ghc=8.4.4+dfsg1-3 \
      cabal-install=2.2.0.0-2 \
      git=1:2.20.1-2+deb10u1 \
      ca-certificates=20190110

# Build from source
RUN git clone https://github.com/koalaman/shellcheck/
WORKDIR /opt/shellcheck
RUN git checkout v0.7.0 &&\
    git log --show-signature -1 && \
    cabal update && \
    cabal install \
      --dependencies-only \
      --ghc-options="-optlo-Os -split-sections" && \
    cabal build Paths_ShellCheck && \
    ghc \
      -optl-static \
      -optl-pthread \
      -isrc \
      -idist/build/autogen \
      --make shellcheck \
      -split-sections \
      -optc-Wl,--gc-sections \
      -optlo-Os && \
    strip --strip-all shellcheck && \
    mkdir -p /out && \
    cp shellcheck /out/

FROM alpine:3.11
WORKDIR /data
COPY --from=build /out /bin/
RUN rm -rf /etc/apk/repositories && \
    apk add --no-cache \
      tini==0.18.0-r0  \
      --repository https://alpine.global.ssl.fastly.net/alpine/v3.11/main \
      --repository https://alpine.global.ssl.fastly.net/alpine/v3.11/community && \
    apk info -a tini && \
    addgroup -g 1000 -S shellcheck && \
    adduser -u 1000 -S shellcheck -G shellcheck
USER shellcheck
ENV SHELLCHECK_OPTS=""

ENTRYPOINT ["/sbin/tini", "--", "shellcheck" ]
CMD ["--help"]

LABEL org.label-schema.name="shellcheck" \
      org.label-schema.description="--" \
      org.label-schema.vendor="--" \
      org.label-schema.docker.schema-version="1.0" \
      org.label-schema.vcs-type="git"
