path := $(abspath $(lastword $(MAKEFILE_LIST)))
dir := $(notdir $(patsubst %/,%,$(dir $(path))))

pull:
	docker pull debian:stable
	docker pull alpine:3.11

lint:
	pre-commit run --all-files --verbose || true
	yamllint .
	markdownlint .
	for FILE in $(find . -iname '*.json'); do echo "${FILE}"; jsonlint $FILE --quiet --compact; done

build:
	docker buildx build --tag bestops/$(dir):latest --platform linux/amd64 .

build-multi:
	docker buildx create --use --name multi-arch-builder || true
	docker buildx build --tag bestops/$(dir):latest --platform linux/amd64,linux/arm64,linux/s390x,linux/386,linux/arm/v7,linux/arm/v6 .

test:
	docker buildx create --use --name multi-arch-builder || true
	docker buildx build --tag bestops/$(dir):test --platform linux/amd64 .
	docker buildx imagetools inspect bestops/$(dir):test

prettier:
	# npm install -g prettier
	prettier --parser=markdown --write '*.md' '**/*.md'
	prettier --parser=json --write '*.json' '**/*.json'
	prettier --parser=yaml --write '*.y*ml' '**/*.y*ml'
	git status

version-patch:
	bump2version patch --commit --tag
	# --sign-tags

version-minor:
	bump2version minor --commit --tag
	# --sign-tags

version-major:
	bump2version major --commit --tag
	# --sign-tags

version-release:
	bump2version release --commit --tag
	# --sign-tags

version-build:
	bump2version build --commit --tag
	# --sign-tags

