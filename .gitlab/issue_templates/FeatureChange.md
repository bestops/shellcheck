# Feature Change Request

## Change cause

What is the cause of the change (e.g., DEFECT, UPDATE, UPGRADE).

## Further details

Include use cases, benefits, and/or goals (contributes to our vision?)

## Documentation

Required documentation:

## Testing

What risks does this change pose? How might it affect the quality of the product? What additional test coverage or changes to tests will be needed? Will it require cross-browser testing?

## Definition of Done

Define both the success metrics and acceptance criteria. Note that success metrics indicate the desired business outcomes, while acceptance criteria indicate when the solution is working correctly. If there is no way to measure success, link to an issue that will implement a way to measure this.

## Links / references

Links and references, or "No links nor references".

/label ~feature ~change-request
