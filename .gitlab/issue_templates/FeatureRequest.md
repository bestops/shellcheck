# Feature Request <FEATURE_NO>

## Problem to solve

What problem do we solve?

## Intended users

Who will use this feature? If known, include any of the following: types of users (e.g. Developer), personas, or specific company roles (e.g. Release Manager). It's okay to write "Unknown" and fill this field in later.

## Further details

Include use cases, benefits, and/or goals (contributes to our vision?)

## Proposal

How are we going to solve the problem? Try to include the user journey!

## Permissions and Security

What permissions are required to perform the described actions? Are they consistent with the existing permissions as documented for users, groups, and projects as appropriate? Is the proposed behavior consistent between the UI, API, and other access methods (e.g. email replies)?

## Documentation

Required documentation:

## Testing

What risks does this change pose? How might it affect the quality of the product? What additional test coverage or changes to tests will be needed? Will it require cross-browser testing?

## Definition of Done

Define both the success metrics and acceptance criteria. Note that success metrics indicate the desired business outcomes, while acceptance criteria indicate when the solution is working correctly. If there is no way to measure success, link to an issue that will implement a way to measure this.

## Links / references

Links and references, or "No links nor references".

/label ~feature
