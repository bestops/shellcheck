# Release Notes

- Version: `1.0.0`
- Release Date: Thu, Oct 25, 2019

## Checklist

_The release MUST be inmediately aborted if a problem occurs during the release process, and the package SHOULD be rolled-back to the previous available version._

- Release is agreed with and/or authorized by the customer: **Yes/No**
- Release SHOULD be performed within a fixed time window: : **Yes/No**
- Release involves service interruption: **Yes/No**
- Release involves data or service migrations: **Yes/No**
- Release requires additional assistance (e.g., DevOps): **Yes/No**
- Release SHOULD be tagged after merge: **Yes/No**

_The release CHANGELOG follows; all items refer to the corresponding backlog._ _If any section is empty it MUST be explicitly stated (e.g. “No features added in this release”), and the corresponding table of items should be removed._

## Added

_The following list of features are to be added in this version; or the text "No features added in this release.":_

- (Ref. Item) Feature summary [branch]

## Changed

_The following list of features changed respect to previous versions; or the text "No features changed in this release.":_

- (Ref. Item) Feature summary [branch]

## Deprecated

_The following list of features are being deprecated in this release, and will be removed in a near future; or the text "No features deprecated in this release.":_

- (Ref. Item) Feature summary [branch]

## Removed

_The following list of features are removed in this version; or the text "No features removed in this release.":_

- (Ref. Item) Feature summary [branch]

## Fixed

_The following of bugfixes and issues are fixed in this version; or the text "No issues fixed in this release.":_

- (Ref. Item) Feature summary [branch]

## Security

_The following list of vulnerabilities are fixed in this version; or the text "No security vulnerabilities fixed in this release.":_

- (Ref. Item) Feature summary [branch]

## Additional Notes

Optional additional notes; or the text "No additional notes."

/label ~release
